module EditLinkHelper
  # Returns `sites/<site>` if page lives in a monorepo site, empty string if not
  def monorepo_site_path_fragment
    monorepo_site = nil
    data.monorepo_sites.each do |site, entry|
      entry.paths.each do |path|
        site_path_regex = /^#{path}/
        if current_page.file_descriptor.relative_path.to_s.match?(site_path_regex)
          monorepo_site = site
        end
      end
    end
    monorepo_site ? "sites/#{monorepo_site}/" : ''
  end
end
