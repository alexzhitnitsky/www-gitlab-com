
<!--

This template is for filing a bug report related to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/).

This is not the correct repository for requests related to docs.gitlab.com, the GitLab product, or other items.

For information regarding working on the about website please visit the [Brand and Digital Handbook](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/)

You may link to this issue template using the following URL: https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report

-->

#### What is/are the relevant URLs or emails?

<!--
Example: https://about.gitlab.com/something/etc/
-->

#### Briefly describe the bug

<!--
Example: A link is broken and when hovering over it the color is the wrong color and poorly aligned.
-->

#### What are the steps to reproduce the bug

<!--
Example (In a bulleted list):
* Scroll to the W section of the page
* When condition X is true
* Hover over Y
-->

#### Please provide any relevant screenshots or screencasts

<!--
[How to take a screenshot](https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/)
-->

#### What is your window size, browser, operating system, and versions

<!--
Please use the following website to determine your browser specifications and provide a link developers can follow
[What is my browser](https://www.whatsmybrowser.org)
Example: Chrome 77, macOS 10.15.0, 944px by 977px, https://whatsmybrowser.org/b/MPC757K
-->

#### What device are you using

<!--
Example: Samsung Galaxy S 10 Plus
-->

#### Have you tried a fresh incognito window? Could this be related to cookies or account type/state?

<!--
Examples:
* I tried a fresh incognito window & it had no impact.
* The problem goes away when using an incognito window.
* It only happens when YYYYYY cookies are set to ZZZZZZ.
-->

#### What browser plugins do you have enabled?

<!--
Example: Adblock, noscript, ghostery, safe browsing.
Why?: Certain website bugs may be caused by plugins.
-->

#### What is your geographic location

<!--
Why?: Certain bugs may be geographically related. For example, if you're in the European Union, it could be related to GDPR policy and cookies.
-->

#### What type of network are you connected to?

<!--
Examples:
* Wired corporate network.
* Wifi at home.
* Restaurant wifi.
* AT&T 4G.
-->

<!-- If you do not want to ping the website team, please remove the following cc -->
/cc @gl-website

<!-- These labels will be automatically applied unless you edit or delete the following section -->
/label ~"mktg-website" ~"template::web-bug" ~"mktg-status::triage"
