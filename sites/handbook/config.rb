# rubocop:disable Style/RegexpLiteral

monorepo_root = File.expand_path('../..', __dir__)

#--------------------------------------------------
# Only include this site's resources in this config
#--------------------------------------------------

# TODO: Abstract this out once we switch the blog to be an independent build
class OnlyHandbookResources < Middleman::Extension
  # We must ensure that this extension runs last, so that the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def manipulate_resource_list(resources)
    resources.select do |resource|
      site = :handbook
      # Get site paths from '/data/monorepo_sites.yml' config file and turn into regexes anchored to beginning of line
      # NOTE: we have to read the YAML file ourselves, we can't rely on @app.data.  See https://github.com/middleman/middleman/issues/1810
      monorepo_site_path_regexes = monorepo_sites.fetch(site).fetch(:paths).map { |path| %r{^#{path}} }

      # Add in a regex for sitemap.xml
      destination_path_regexes = monorepo_site_path_regexes + [%r{^sitemap.xml}]

      # Include the resource only if its destination_path matches any of the regexes
      destination_path_regexes.any? do |regex|
        resource.destination_path =~ regex
      end
    end
  end

  private

  def monorepo_sites
    @monorepo_sites ||= Psych.load(File.read(File.expand_path('../../data/monorepo_sites.yml', __dir__)), symbolize_names: true)
  end
end
::Middleman::Extensions.register(:only_handbook_resources, OnlyHandbookResources)
activate :only_handbook_resources

#-----------------------------------------------------------------------------
# Make top-level shared files used by this site available to this site's Middleman build
#-----------------------------------------------------------------------------

set :layout, 'layout' # For some reason this has to be made explicit in the sub-site build, even though it's the default
set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

# Get site paths from '/data/monorepo_sites.yml' config file and turn into regexes anchored to beginning of line
# NOTE: we have to read the YAML file ourselves, we can't rely on data.  See https://github.com/middleman/middleman/issues/1810
monorepo_sites = Psych.load(File.read(File.expand_path('../../data/monorepo_sites.yml', __dir__)), symbolize_names: true)

# Map shared files to regexes to be used in files.watch
shared_files = monorepo_sites.fetch(:handbook).fetch(:shared_files).map { |f| %r{#{f}} }
files.watch(
  :source,
  path: File.expand_path("#{monorepo_root}/source", __dir__),
  only: shared_files
)

#----------------------------
# End of common file handling
#----------------------------

#---------------------------------------
# Rest of middleman config for this site
#---------------------------------------

# hack around relative requires elsewhere in the common code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

require_relative "../../extensions/breadcrumbs"
require_relative "../../extensions/codeowners"
require_relative "../../lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :codeowners

# Build-specific configuration
configure :build do
  set :build_dir, "#{monorepo_root}/public"

  # NOTE: Currently no css or js in individual sites, it should all be at top level

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup
end

# TODO: Add this back when rest of handbook besides /marketing and /engineering is moved to sites/handbook/source
# # Hiring chart pages
# Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
#   proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
# end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Don't include the following into the sitemap
ignore '**/.gitkeep'

# rubocop:enable Style/RegexpLiteral
