---
layout: handbook-page-toc
title: "Inbound Marketing"
---

## Inbound Marketing

The Inbound Marketing team focuses on helping grow revneue for GitLab by increasing traffic to GitLab public websites and improving conversion paths for visitors to become MQLs (Marketing Qualified Leads). We use research and data to help improve organic discovery and make sure our offers match visitor expectations across about.gitlab.com.

## Meet the Inbound Marketing team

[**Shane Rice**](https://about.gitlab.com/company/team/#shanerice)

* Title: Manager, Inbound Marketing
* GitLab handle: shanerice
* Slack handle: @shanerice

[**Niall Cregan**](https://about.gitlab.com/company/team/#niallcregan)

* Title: Inbound Marketing Associate
* GitLab handle: ncregan
* Slack handle: @Niall Cregan

[**Hanif Smith-Watson**](https://about.gitlab.com/company/team/#hsmith-watson)
* Title: Inbound Marketing Manager
* GitLab handle: hsmith-watson
* Slack handle: @Hanif Smith-Watson 

## Areas we focus
* Organic discovery
  * site health for about.gitlab.com
  * keyword research
  * search architecture
* [Marketing website analytics](/handbook/marketing/growth-marketing/inbound-marketing/analytics/)
* Conversion path optimization
  * on page CTAs (call to action)
  * A/B testing

### Organic discovery
Most people entering gitlab.com and all sub-domains start their journey on a search engine. Our team's goal is to help them discover the most relevant and accurate match for their query.

We don't engage in any dark patterns to trick people or search engine bots. We focus on improving the technical signals our site sends by limiting broken pages, adding redirects, updating metadata, removing duplicate content, and providing relevant research to teams across GitLab. The rest of this page is devoted to the processes we follow to improve overall organic discovery.

### Links on about.gitlab.com

We should link to resources that will help our readers. Be sure to include links to blog posts, guides, and other reference material. You can also include links to company or product websites if they are relevant to your topic. These links do not need to be "nofollowed" if they are informational.

However, we should use [Google's guidelines on nofollowing links](https://webmasters.googleblog.com/2016/03/best-practices-for-bloggers-reviewing.html) when we exchange a link for a product or service. It's also a best practice to ask for a nofollow link when we sponsor and disclose our links to sponsored content.

### Website Health Check

Regular website health checks should be performed. These checks are meant to ensure that there are no issues with the website that could cause issues with traffic to the site. These are the following things to check to ensure the health of the site:

- [Google Search Console](https://www.google.com/webmasters/tools/)
- [Google Analytics](https://analytics.google.com/analytics/web/)

Issues to look for in each of these tools:

- **Google Search Console**: Check the dashboard and messages for any important notifications regarding the website. Also check under `Search Traffic` > `Manual Actions` for any URLs that have been identified as spam or harmful content. Forward security warnings to the Abuse team and follow the [DMCA complaint process](/handbook/support/workflows/dmca.html) with the support team.
- **Google Analytics**: Compare organic site traffic from the most previous week compared to the previous week and look for any large fluctuations.

### Using robots metadata to manage search index

There are times we need to keep pages out of search indexes. For example, we might duplicate most of a page to improve conversion for an ad campaign. It's relatively rare to use this, but it's an important tool that helps us increase organic reach and paid advertising efficiency.

All pages are set to `meta name="robots" content="index, follow"` by default. To exclude a page from the index add `noindex: true` to the frontmatter, and this will set the robots metadata to `meta name="robots" content="noindex, follow"`.

### about.gitlab.com redirects

Occasionally we need to change URL structures of the website, and we want to make sure that people can find pages they need. We use 301 redirects to send people to the right URL when it's appropriate. 

#### about.gitlab.com redirect policy

We will redirect URLs included in Google's search index. A simple test to see if a page is indexed is to search for the URL with a site modifier, `site:url`, using Google.

We want to reduce the number of internal redirects, which means we need to update links across about.gitlab.com when we change URLs. When you request a redirect please indicate whether or not you were able to search across about.gitlab.com and update the links for a page you're moving.

#### Request an about.gitlab.com redirect

The Digital Marketing Programs team can set up and manage all redirects for about.gitlab.com.

To redirect an outdated page, open an issue with the [set up a new redirect template in the Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/issues/new?issuable_template=set-up-a-new-redirect). You'll need to provide the following:

- Old URL that needs to be redirected
- New URL where users should now be sent
- Were you able to update existing links to the old URL across about.gitlab.com?

If you have any questions or concerns regarding your redirect request, ask for help in the `#digital-marketing` channel on Slack.

#### Redirect process documentation

The Digital Marketing Programs team uses these [technical details for the redirect process](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/redirects_on_about_gitlab_com.md) on about.gitlab.com.
