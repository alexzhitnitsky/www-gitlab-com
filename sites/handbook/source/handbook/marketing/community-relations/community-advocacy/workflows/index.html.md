---
layout: handbook-page-toc
title: "Community advocacy workflows"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Community response workflows

- [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews)
- [Code of Conduct Enforcement](/handbook/marketing/community-relations/community-advocacy/workflows/code-of-conduct-enforcement)
- [Education, Open Source and Startup programs](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup)
- [Twitter](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)
- [Website comments](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments)
- [E-mail](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail)
- [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/reddit)
- [Stack Overflow](/handbook/marketing/community-relations/community-advocacy/workflows/stackoverflow)
- [GitLab Forum](/handbook/marketing/community-relations/community-advocacy/workflows/forum)
- [Facebook](/handbook/marketing/community-relations/community-advocacy/workflows/facebook/)
- [Inactive workflows](/handbook/marketing/community-relations/community-advocacy/workflows/inactive)

## Other workflows

- [Involving experts](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts)
- [Knowledge base](/handbook/marketing/community-relations/community-advocacy/workflows/knowledge-base)
- [#movingtogitlab](/handbook/marketing/community-relations/community-advocacy/workflows/moving-to-gitlab)
- [Advocate for a Day](/handbook/marketing/community-relations/community-advocacy/workflows/advocate-for-a-day)
- [Expertise workflow rotation](/handbook/marketing/community-relations/community-advocacy/workflows/expertise-rotation)
- [Merchandise handling](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling)
- [Release day duties](/handbook/marketing/community-relations/community-advocacy/workflows/release-duties)
