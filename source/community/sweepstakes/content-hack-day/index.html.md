---
layout: markdown_page
title: Content Hack Day April 2018
---

## OFFICIAL RULES

NO PURCHASE NECESSARY TO ENTER OR BE SELECTED. MAKING A PURCHASE OR PAYMENT OF ANY KIND
WILL NOT INCREASE YOUR CHANCES OF SELECTION. VOID WHERE PROHIBITED OR RESTRICTED
BY LAW.

1.PROMOTION DESCRIPTION: The **CONTENT HACK DAY** (\"Sweepstakes\") begins on
**APRIL 16 at 11 AM PACIFIC TIME**  and ends on **APRIL 22 at 11:59 PM PACIFIC TIME** (the \"Promotion Period\").

    The sponsor of this Sweepstakes is GITLAB ("Sponsor"). By participating in
    the Sweepstakes, each Entrant unconditionally accepts and agrees to comply
    with and abide by these Official Rules and the decisions of Sponsor, which
    shall be final and binding in all respects. Sponsor is responsible for the
    collection, submission or processing of Entries and the overall administration
    of the Sweepstakes. Entrants should look solely to Sponsor with any questions, c
    omments or problems related to the Sweepstakes. Sponsor may be reached by
    email at GIVEAWAYS@GITLAB.COM during the Promotion Period.

2.ELIGIBILITY: Open to legal residents of WORLDWIDE who are 18 or older (the \"Entrant\").
Sponsor, and their respective parents, subsidiaries, affiliates, distributors,
retailers, sales representatives, advertising and promotion agencies and each of
their respective officers, directors and employees (the \"Promotion Entities\"),
are ineligible to enter the Sweepstakes or win a prize. Household Members and
Immediate Family Members of such individuals are also not eligible to enter or win.
"Household Members" shall mean those people who share the same residence at least
three months a year. "Immediate Family Members" shall mean parents, step-parents,
legal guardians, children, step-children, siblings, step-siblings, or spouses.
This Sweepstakes is subject to all applicable federal, state and local laws and
regulations and is void where prohibited or restricted by law.

3.PRIZES:

As a reward for taking part, the 10 selected participants
will each receive one (1) GitLab notebook and one (1) GitLab recycled pen. The participant who submits the most complete blog posts as part of the Hack Day (where a complete blog post consists of a merge request or Google document with a finished final draft submitted by MAY 4 at 11:59 PM Pacific Time) will be awarded a prize of one (1) set of GitLab noise-canceling headphones. Sponsor reserves the
right in its sole and absolute discretion to award a substitute reward of equal
or greater value if a reward described in these Official Rules is unavailable or
cannot be awarded, in whole or in part, for any reason. The ARV of the reward
represents Sponsor's good faith determination. That determination is final and
binding and cannot be appealed. If the actual value of the reward turns out to be
less than the stated ARV, the difference will not be awarded in cash. Sponsor
makes no representation or warranty concerning the appearance, safety or performance
of any reward awarded. Restrictions, conditions, and limitations may apply.
Sponsor will not replace any lost or stolen prize items.

This Sweepstakes is open to legal residents of WORLDWIDE and the reward will only be
awarded and/or delivered to addresses within said locations. All federal, state
and/or local taxes, fees, and surcharges are the sole responsibility of the participants.
Failure to comply with the Official Rules will result in forfeiture of the reward.

4.HOW TO ENTER: Community members are required to submit two relevant writing samples
to `content@gitlab.com` no later than APRIL 22 for assessment.

Automated or robotic Entries submitted by individuals or organizations will be disqualified.
Internet entry must be made by the Entrant. Any attempt by Entrant to obtain more
than the stated number of Entries by using multiple/different email addresses,
identities, registrations, logins or any other methods, including, but not limited
to, commercial contest/sweepstakes subscription notification and/or entering services,
will void Entrant's Entries and that Entrant may be disqualified. Final eligibility
for the award of any prize is subject to eligibility verification as set forth below.
All Entries must be posted by the end of the Promotion Period in order to participate.
Sponsor's database clock will be the official timekeeper for this Sweepstakes.

No cost is expected to be incurred for participating.

5.PARTICIPANT SELECTION: Submissions will be reviewed by the GitLab
[blog editorial team](/handbook/marketing/blog/#blog-editorial-team)
and judged based on writing style, skill, and subject matter expertise. A maximum
number of 10 entrants will be selected to participate in Content Hack Day.

6.PARTICIPANT NOTIFICATION: Successful entrants will be notified by email at the email address
provided in the Entry Information approximately 3 days prior to Content Hack Day.
Potential Winner must accept a prize by email as directed
by Sponsor within WINNER ACCEPTANCE TIMEFRAME of notification. Sponsor is not
responsible for any delay or failure to receive notification for any reason,
including inactive email account(s), technical difficulties associated therewith,
or Winner’s failure to adequately monitor any email account.

Any notification not responded to or returned as undeliverable may result
in reward forfeiture. The potential successful entrants may be required to sign and return
an affidavit of eligibility and release of liability, and a Publicity Release
(collectively \"the Prize Claim Documents\"). No substitution or transfer of a
prize is permitted except by Sponsor.

7.PRIVACY: Any personal information supplied by you will be subject to the
privacy policy of the Sponsor posted at /privacy/.
By entering the Sweepstakes, you grant Sponsor permission to share your email
address and any other personally identifiable information with the other Sweepstakes
Entities for the purpose of administration and prize fulfilment, including use
in a publicly available Winners list.

8.LIMITATION OF LIABILITY: Sponsor assumes no responsibility or liability for
(a) any incorrect or inaccurate entry information, or for any faulty or failed
electronic data transmissions; (b) any unauthorized access to, or theft, destruction
or alteration of entries at any point in the operation of this Sweepstakes; (c)
any technical malfunction, failure, error, omission, interruption, deletion, defect,
delay in operation or communications line failure, regardless of cause, with regard
to any equipment, systems, networks, lines, satellites, servers, camera, computers
or providers utilized in any aspect of the operation of the Sweepstakes;
(d) inaccessibility or unavailability of any network or wireless service, the
Internet or website or any combination thereof; (e) suspended or discontinued Internet,
wireless or landline phone service; or (f) any injury or damage to participant's
or to any other person’s computer or mobile device which may be related to or
resulting from any attempt to participate in the Sweepstakes or download of any
materials in the Sweepstakes.

If, for any reason, the Sweepstakes is not capable of running as planned for reasons
which may include without limitation, infection by computer virus, tampering,
unauthorized intervention, fraud, technical failures, or any other causes which
may corrupt or affect the administration, security, fairness, integrity or proper
conduct of this Sweepstakes, the Sponsor reserves the right at its sole discretion
to cancel, terminate, modify or suspend the Sweepstakes in whole or in part.
In such event, Sponsor shall immediately suspend all drawings and prize awards,
and Sponsor reserves the right to award any remaining prizes (up to the total ARV
as set forth in these Official Rules) in a manner deemed fair and equitable by Sponsor.
Sponsor and Released Parties shall not have any further liability to any participant in connection with the Sweepstakes.

9.SOCIAL NETWORK DISCLAIMER
An account with one of the following social networks may be required to enter: Twitter, Facebook, LinkedIn, or Google+.
If you don’t already have an account, visit www.facebook.com, www.twitter.com, www.linkedin.com, or www.plus.google.com/create to create one.
It is free to create an account. This promotion is in no way sponsored, endorsed
or administered by, or associated with Facebook. You understand that you are
providing your information to the Sponsor and not to Facebook. By participating
via the Facebook platform, participants are also subject to Facebook’s data policy
and terms of use, which can be found at https://www.facebook.com/about/privacy
and https://www.facebook.com/legal/terms/update.

10.WINNER LIST/OFFICIAL RULES: To obtain a copy of the Winner List or a copy of
these Official Rules, send your request along with a stamped, self-addressed
envelope to GITLAB cc: **Content Hack Day April 2018** at 268 Bush Street #350, SAN FRANCISCO, CA 94104.
Requests for the names of the winners must be received no later than **MAY 31, 2018**
after the Promotion Period has ended. For accessing a Winner List online, visit this page **/sweepstakes/content-hack-day/**.
The winner list will be posted after winner confirmation is complete.

11.SPONSOR:

GITLAB

268 Bush Street #350,

 SAN FRANCISCO, CA 94104

GIVEAWAYS@GITLAB.COM
